﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog5.Models
{
    public class Post
    {
        public int ID { set; get; }
        public String Title { set; get; }
        public String Body { set; get; }
        public DateTime DateCreated { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}