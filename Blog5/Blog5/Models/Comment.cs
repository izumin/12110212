﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog5.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public String Title { set; get; }
        public String Body { set; get; }
        public DateTime DateCreated { set; get; }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
    }
}